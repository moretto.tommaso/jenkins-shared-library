#!/usr/bin/env groovy

package com.example

class Maven implements Serializable{
    def script

    Maven(script) {
        this.script = script
    }

    def buildJAR() {
        script.echo "building the application for branch $script.BRANCH_NAME"
        script.sh 'mvn clean package'
    }

    def testing() {
        script.echo "testing the application for branch $script.BRANCH_NAME ..."
        script.sh 'mvn test'
    }

    def incrementVersion() {
        script.echo "incrementing the app version for branch $script.BRANCH_NAME ..."
        script.sh ' mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit'
        def matcher = script.readFile('pom.xml') =~ '<version>(.+)</version>'
        def version = matcher[0][1]
        return "$version-$script.BUILD_NUMBER"
    }
}